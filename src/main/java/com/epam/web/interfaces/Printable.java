package com.epam.web.interfaces;

@FunctionalInterface
public interface Printable {
    String print();
}
