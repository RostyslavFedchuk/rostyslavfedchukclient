package com.epam.web.interfaces;

import java.util.Map;

public interface ClientManager {
    Map<String, Printable> getMenuMethods();

    void printMenu();
}
