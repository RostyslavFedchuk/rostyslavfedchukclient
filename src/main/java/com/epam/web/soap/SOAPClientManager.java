package com.epam.web.soap;

import com.epam.web.interfaces.ClientManager;
import com.epam.web.Menu;
import com.epam.web.interfaces.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class SOAPClientManager implements ClientManager {
    private static Logger logger = LogManager.getLogger(SOAPClientManager.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    private CalculatorSOAP calculator;

    public Map<String, Printable> getMenuMethods() {
        return menuMethods;
    }

    public SOAPClientManager() {
        menu = Menu.getMenu("SOAPMenu");
        setMenuMethods();
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("config");
            URL wsdlUrl = new URL("http://localhost:" + bundle.getString("port") + "/webServices/calculateSOAP?wsdl");
            QName qName = new QName("http://soap.web.epam.com/", "CalculatorSOAPServiceService");
            Service service = Service.create(wsdlUrl, qName);
            calculator = service.getPort(CalculatorSOAP.class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void setMenuMethods() {
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::addValues);
        menuMethods.put("2", this::subtractValues);
        menuMethods.put("3", this::divideValues);
        menuMethods.put("4", this::multiplyValues);
        menuMethods.put("5", this::percent);
        menuMethods.put("6", this::round);
        menuMethods.put("7", this::pow);
        menuMethods.put("Q", this::quit);
    }

    public void printMenu() {
        logger.trace("____________________"
                + "SOAP CLIENT MENU____________________\n");
        for (Map.Entry entry : menu.entrySet()) {
            logger.trace(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        logger.trace("________________________________________\n");
    }

    private String addValues() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return "Result: " + split[0] + " + " + split[1] + " = "
                    + calculator.add(Double.valueOf(split[0]), Double.valueOf(split[1]));
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String subtractValues() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return "Result: " + split[0] + " - " + split[1] + " = "
                    + calculator.subtract(Double.valueOf(split[0]), Double.valueOf(split[1]));
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String divideValues() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return "Result: " + split[0] + " / " + split[1] + " = "
                    + calculator.divide(Double.valueOf(split[0]), Double.valueOf(split[1]));
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String multiplyValues() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return "Result: " + split[0] + " * " + split[1] + " = "
                    + calculator.multiply(Double.valueOf(split[0]), Double.valueOf(split[1]));
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String pow() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+)")) {
            String[] split = value.split(";");
            return "Result: " + split[0] + " ^ " + split[1] + " = "
                    + calculator.pow(Double.valueOf(split[0]), Integer.valueOf(split[1]));
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String percent() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return "Result: " + split[0] + " percent " + split[1] + " = "
                    + calculator.percent(Double.valueOf(split[0]), Double.valueOf(split[1]));
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String round() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+)")) {
            String[] split = value.split(";");
            return split[0] + " is Rounded to "+split[1]+" digits: " + calculator.round(Double.valueOf(split[0]), Integer.valueOf(split[1]));
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String quit() {
        return "Bye!!\n";
    }
}
