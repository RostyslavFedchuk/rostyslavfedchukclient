package com.epam.web;

import com.epam.web.interfaces.ClientManager;
import com.epam.web.rest.RESTClientManager;
import com.epam.web.soap.SOAPClientManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ClientRunner {
    private static final Scanner SCANNER = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(ClientRunner.class);
    private ClientManager manager;

    public void runClient(WebClient client){
        if(client == WebClient.REST){
            manager = new RESTClientManager();
        } else if(client == WebClient.SOAP){
            manager = new SOAPClientManager();
        }
        start();
    }

    private void start(){
        String option = "";
        do {
            manager.printMenu();
            logger.trace("Choose one option: ");
            option = SCANNER.next().toUpperCase();
            if(manager.getMenuMethods().containsKey(option)){
                logger.info(manager.getMenuMethods().get(option).print()+"\n");
            }else {
                logger.info("Wrong input! Try again.\n");
            }
        } while (!option.equals("Q"));
    }
}
