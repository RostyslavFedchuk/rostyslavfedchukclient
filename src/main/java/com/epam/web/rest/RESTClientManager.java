package com.epam.web.rest;

import com.epam.model.MathAction;
import com.epam.web.interfaces.ClientManager;
import com.epam.web.Menu;
import com.epam.web.interfaces.Printable;
import com.epam.web.soap.SOAPClientManager;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.MappingJsonFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class RESTClientManager implements ClientManager {
    private static final Scanner SCANNER = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(SOAPClientManager.class);
    private static String endpointUrl = ResourceBundle.getBundle("config").getString("endpointUrl");
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;


    public Map<String, Printable> getMenuMethods() {
        return menuMethods;
    }

    public RESTClientManager() {
        menu = Menu.getMenu("RESTMenu");
        setMenuMethods();
    }

    private void setMenuMethods() {
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::addValues);
        menuMethods.put("2", this::subtractValues);
        menuMethods.put("3", this::divideValues);
        menuMethods.put("4", this::multiplyValues);
        menuMethods.put("5", this::percent);
        menuMethods.put("6", this::pow);
        menuMethods.put("7", this::getLastResult);
        menuMethods.put("8", this::getLastResults);
        menuMethods.put("Q", this::quit);
    }

    public void printMenu() {
        logger.trace("____________________"
                + "REST CLIENT MENU____________________\n");
        for (Map.Entry entry : menu.entrySet()) {
            logger.trace(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        logger.trace("________________________________________\n");
    }

    private MathAction getJSONResult(String action, String firstNumber, String secondNumber) {
        try {
            WebClient client = WebClient.create(endpointUrl + action + "?a="
                    + firstNumber + "&b=" + secondNumber);
            Response response = client.accept("application/json").type("application/json").get();
            JsonParser parser = new MappingJsonFactory().createJsonParser((InputStream) response.getEntity());
            return parser.readValueAs(MathAction.class);
        } catch (IOException e) {
            e.printStackTrace();
            return new MathAction();
        }
    }

    private String addValues() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return getJSONResult("/add",split[0], split[1]).toString();
        } else {
            return "Incorrect input!!\n";
        }
    }


    private String subtractValues() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return getJSONResult("/subtract",split[0], split[1]).toString();
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String divideValues() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return getJSONResult("/divide",split[0], split[1]).toString();
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String multiplyValues() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+(\\.)?[0-9]*)")) {
            String[] split = value.split(";");
            return getJSONResult("/multiply",split[0], split[1]).toString();
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String pow() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+)")) {
            String[] split = value.split(";");
            try {
                WebClient client = WebClient.create(endpointUrl + "/pow?number=" + split[0] + "&pow=" + split[1]);
                Response response = client.accept("text/plain").get();
                return "Result: " + split[0] + " ^ " + split[1] + " = "
                        + IOUtils.toString((InputStream)response.getEntity());
            } catch (IOException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String percent() {
        logger.trace("EXAMPLE '14;34'...\nEnter two numbers: ");
        String value = SCANNER.next();
        if (value.matches("([0-9]+(\\.)?[0-9]*);([0-9]+)")) {
            String[] split = value.split(";");
            try {
                WebClient client = WebClient.create(endpointUrl + "/percent?number=" + split[0] + "&percent=" + split[1]);
                Response response = client.accept("text/plain").get();
                return "Result: " + split[0] + " percent " + split[1] + " = "
                        + IOUtils.toString((InputStream)response.getEntity());
            } catch (IOException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        } else {
            return "Incorrect input!!\n";
        }
    }

    private String getLastResult(){
        try {
            WebClient client = WebClient.create(endpointUrl + "/lastResult");
            Response response = client.accept("application/json").type("application/json").get();
            JsonParser parser = new MappingJsonFactory().createJsonParser((InputStream) response.getEntity());
            return parser.readValueAs(MathAction.class).toString();
        } catch (IOException e) {
            e.printStackTrace();
            return new MathAction().toString();
        }
    }

    private String getLastResults(){
        try {
            logger.info("Enter count of results to print: ");
            WebClient client = WebClient.create(endpointUrl + "/lastResults?count=" + SCANNER.nextInt());
            Response response = client.accept("text/plain").get();
            return IOUtils.toString((InputStream)response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
            return new MathAction().toString();
        }
    }

    private String quit() {
        return "Bye!!\n";
    }
}
