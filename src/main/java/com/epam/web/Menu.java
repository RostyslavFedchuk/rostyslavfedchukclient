package com.epam.web;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class Menu {
    private static Map<String, String> menu;

    private static void setMenu(String propertyName){
        ResourceBundle bundle;bundle = ResourceBundle.getBundle(propertyName);
        final int countOptions = Integer.valueOf(bundle.getString("countOptions"));
        menu = new LinkedHashMap<>();
        for (int i = 1; i <= countOptions; i++) {
            menu.put("" + i, bundle.getString("" + i));
        }
        menu.put("Q", bundle.getString("Q"));
    }

    public static Map<String, String> getMenu(String propertyName){
        setMenu(propertyName);
        return menu;
    }
}
