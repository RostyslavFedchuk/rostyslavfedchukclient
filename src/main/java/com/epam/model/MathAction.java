package com.epam.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class MathAction {
    private double first;
    private double second;
    private String dateTime;
    private double result;
    private String action;

    public MathAction(){}

    public MathAction(double first, double second, double result, String action) {
        this.first = first;
        this.second = second;
        this.result = result;
        this.action = action;
        this.dateTime = LocalDateTime.now().toString();
    }

    public double getFirst() {
        return first;
    }

    public void setFirst(double first) {
        this.first = first;
    }

    public double getSecond() {
        return second;
    }

    public void setSecond(double second) {
        this.second = second;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "MathAction{" +
                "first=" + first +
                ", second=" + second +
                ", dateTime=" + dateTime +
                ", result=" + result +
                ", action='" + action + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathAction that = (MathAction) o;
        return Double.compare(that.first, first) == 0 &&
                Double.compare(that.second, second) == 0 &&
                Double.compare(that.result, result) == 0 &&
                Objects.equals(action, that.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second, result, action);
    }
}
