package com.epam;

import com.epam.utils.EnamUtils;
import com.epam.web.ClientRunner;
import com.epam.web.WebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Scanner;

public class Application {
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final Logger LOGGER = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        start();
    }

    public static void start(){
        String option = "";
        do {
            LOGGER.info("(REST | SOAP | Q) Select which client to create: ");
            option = SCANNER.next().toUpperCase();
            if(EnamUtils.contains(WebClient.values(), option)){
                new ClientRunner().runClient(WebClient.valueOf(option));
            }else {
                LOGGER.info("Wrong input! Try again.\n");
            }
        } while (!option.equals("Q"));
    }
}
