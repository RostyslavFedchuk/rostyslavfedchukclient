package com.epam.web.soap;

import com.epam.utils.CSVReader;
import com.epam.web.rest.CalculatorRESTServiceTest;
import com.epam.web.rest.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import static org.testng.AssertJUnit.assertEquals;

public class CalculatorSOAPServiceTest extends Configuration {
    private static final Logger LOGGER = LogManager.getLogger(CalculatorRESTServiceTest.class);
    private static String endpointUrl = ResourceBundle.getBundle("config").getString("endpointUrl");
    private static CalculatorSOAP calculator;

    @DataProvider(name = "values")
    public static Object[][] getData() {
        LOGGER.info("Using data provider to get parameters from CSV file!\n");
        return CSVReader.readFromCSV("src/main/resources/values.csv", ",");
    }

    @BeforeClass
    public static void init() {
        try {
            URL wsdlUrl = new URL("http://localhost:" + ResourceBundle.getBundle("config")
                    .getString("port") + "/webServices/calculateSOAP?wsdl");
            QName qName = new QName("http://soap.web.epam.com/", "CalculatorSOAPServiceService");
            Service service = Service.create(wsdlUrl, qName);
            calculator = service.getPort(CalculatorSOAP.class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dataProvider = "values")
    public void addTest(String a, String b) {
        double expected = new BigDecimal("" + a).add(new BigDecimal("" + b))
                .setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        double actual = calculator.add(Double.valueOf(a), Double.valueOf(b));
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expected);
        assertEquals(expected, actual);
    }

    @Test(dataProvider = "values")
    public void subtractTest(String a, String b) {
        double expected = new BigDecimal("" + a).subtract(new BigDecimal("" + b))
                .setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        double actual = calculator.subtract(Double.valueOf(a), Double.valueOf(b));
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expected);
        assertEquals(expected, actual);
    }

    @Test(dataProvider = "values")
    public void multiplyTest(String a, String b) {
        double expected = new BigDecimal("" + a).multiply(new BigDecimal("" + b))
                .setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        double actual = calculator.multiply(Double.valueOf(a), Double.valueOf(b));
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expected);
        assertEquals(expected, actual);
    }

    @Test(dataProvider = "values")
    public void divideTest(String a, String b) {
        double actual = calculator.divide(Double.valueOf(a), Double.valueOf(b));
        if (Double.valueOf(b).equals(0.0)) {
            assertEquals(-1.0, actual);
            return;
        }
        double expected = new BigDecimal("" + a)
                .divide(new BigDecimal("" + b), 4, BigDecimal.ROUND_HALF_UP).doubleValue();
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expected);
        assertEquals(expected, actual);
    }

    @Test(dataProvider = "values")
    public void powTest(String a, String b) {
        if (!b.matches("\\d")) {
            return;
        }
        double expected = new BigDecimal("" + a).pow(Integer.valueOf(b))
                .setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        double actual = calculator.pow(Double.valueOf(a), Integer.valueOf(b));
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expected);
        assertEquals(expected, actual);
    }
}
