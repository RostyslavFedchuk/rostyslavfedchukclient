package com.epam.web.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class Configuration {
    private static final Logger LOGGER = LogManager.getLogger(Configuration.class);

    @BeforeSuite
    public static void beforeSuite() {
        LOGGER.info("Hello! We are about to test calculator!\n");
    }

    @BeforeTest
    public static void beforeTest() {
        LOGGER.info("_______________________________<TEST>_______________________________\n");
    }

    @AfterSuite
    public static void afterSuite() {
        LOGGER.info("Test of calculator is over!\n");
    }

    @AfterTest
    public static void afterTest() {
        System.out.println("GD");
        LOGGER.info("_______________________________</TEST>_______________________________\n");
    }
}
