package com.epam.web.rest;

import com.epam.model.MathAction;
import com.epam.utils.CSVReader;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ResourceBundle;

import static org.testng.AssertJUnit.assertEquals;

public class CalculatorRESTServiceTest extends Configuration{
    private static final Logger LOGGER = LogManager.getLogger(CalculatorRESTServiceTest.class);
    private static String endpointUrl = ResourceBundle.getBundle("config").getString("endpointUrl");

    @DataProvider(name = "values")
    public static Object[][] getData() {
        LOGGER.info("Using data provider to get parameters from CSV file!\n");
        return CSVReader.readFromCSV("src/main/resources/values.csv", ",");
    }

    @Test
    public void testPing() {
        WebClient client = WebClient.create(endpointUrl + "/add?a=4&b=15");
        Response response = client.accept("application/json").get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test(dataProvider = "values")
    public void addTest(String a, String b) throws IOException {
        WebClient client = WebClient.create(endpointUrl + "/add?a=" + a + "&b=" + b);
        Response response = client.accept("application/json").type("application/json").get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        double expected = new BigDecimal("" + a).add(new BigDecimal("" + b))
                .setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        MathAction expectedObj = new MathAction(Double.valueOf(a), Double.valueOf(b), expected, "Addition");
        JsonParser parser = new MappingJsonFactory().createJsonParser((InputStream) response.getEntity());
        MathAction actual = parser.readValueAs(MathAction.class);
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expectedObj);
        assertEquals(expectedObj, actual);
        getLastResultTest(expectedObj);
    }

    @Test(dataProvider = "values")
    public void subtractTest(String a, String b) throws IOException {
        WebClient client = WebClient.create(endpointUrl + "/subtract?a=" + a + "&b=" + b);
        Response response = client.accept("application/json").type("application/json").get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        double expected = new BigDecimal("" + a).subtract(new BigDecimal("" + b))
                .setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        MathAction expectedObj = new MathAction(Double.valueOf(a), Double.valueOf(b), expected, "Subtraction");
        JsonParser parser = new MappingJsonFactory().createJsonParser((InputStream) response.getEntity());
        MathAction actual = parser.readValueAs(MathAction.class);
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expectedObj);
        assertEquals(expectedObj, actual);
        getLastResultTest(expectedObj);
    }

    @Test(dataProvider = "values")
    public void multiplyTest(String a, String b) throws IOException {
        WebClient client = WebClient.create(endpointUrl + "/multiply?a=" + a + "&b=" + b);
        Response response = client.accept("application/json").type("application/json").get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        double expected = new BigDecimal("" + a).multiply(new BigDecimal("" + b))
                .setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        MathAction expectedObj = new MathAction(Double.valueOf(a), Double.valueOf(b), expected, "Multiplication");
        JsonParser parser = new MappingJsonFactory().createJsonParser((InputStream) response.getEntity());
        MathAction actual = parser.readValueAs(MathAction.class);
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expectedObj);
        assertEquals(expectedObj, actual);
        getLastResultTest(expectedObj);
    }

    @Test(dataProvider = "values")
    public void divideTest(String a, String b) throws IOException {
        WebClient client = WebClient.create(endpointUrl + "/divide?a=" + a + "&b=" + b);
        Response response = client.accept("application/json").type("application/json").get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        JsonParser parser = new MappingJsonFactory().createJsonParser((InputStream) response.getEntity());
        MathAction actual = parser.readValueAs(MathAction.class);
        if (Double.valueOf(b).equals(0.0)) {
            Assert.assertEquals(actual.getResult(), -1.0);
            return;
        }
        double expected = new BigDecimal("" + a)
                .divide(new BigDecimal("" + b), 4, BigDecimal.ROUND_HALF_UP).doubleValue();
        MathAction expectedObj = new MathAction(Double.valueOf(a), Double.valueOf(b), expected, "Division");
        LOGGER.trace("ACTUAL: " + actual + "\nEXPECTED: " + expectedObj);
        assertEquals(expectedObj, actual);
        getLastResultTest(expectedObj);
    }

    @Test(dataProvider = "values")
    public void powTest(String a, String b) throws IOException {
        if (!b.matches("\\d")) {
            return;
        }
        WebClient client = WebClient.create(endpointUrl + "/pow?number=" + a + "&pow=" + b);
        Response response = client.accept("text/plain").get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        String actual = IOUtils.toString((InputStream) response.getEntity());
        double expected = new BigDecimal("" + a).pow(Integer.valueOf(b))
                .setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        String expectedStr = String.format("Action: %f^%d = %.4f", Double.valueOf(a), Integer.valueOf(b), expected);
        assertEquals(expectedStr, actual);
    }

    public void getLastResultTest(MathAction expected) throws IOException {
        WebClient client = WebClient.create(endpointUrl + "/lastResult");
        Response response = client.accept("application/json").type("application/json").get();
        JsonParser parser = new MappingJsonFactory().createJsonParser((InputStream) response.getEntity());
        MathAction actual = parser.readValueAs(MathAction.class);
        LOGGER.info("Waitin for: " + expected + "\nActual have: " + actual);
        assertEquals(expected, actual);
    }
}
